# GitLab group protected environments can be imported using an id made up of `groupId:environmentName`, e.g.
terraform import gitlab_group_protected_environment.bar 123:production
